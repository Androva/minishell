/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_struct.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/19 16:57:28 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/28 10:04:05 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	init_command(t_command *command)
{
	command->echo = 0;
	command->cd = 0;
	command->exit = 0;
	command->setenv = 0;
	command->unsetenv = 0;
	command->env = 0;
	command->pwd = 0;
	command->other = NULL;
	command->error = 0;
}

void	free_other(t_command *command)
{
	if (command->other != NULL)
		free(command->other);
	command->other = NULL;
}
