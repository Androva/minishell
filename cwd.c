/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cwd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 09:42:14 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/27 15:47:47 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	cwd(t_env *env)
{
	t_env	*current;
	char	cdir[1024];
	char	*oldpwd;

	current = env;
	getcwd(cdir, 1024);
	while (ft_strncmp(current->value, "PWD", 3) != 0)
		current = current->next;
	oldpwd = ft_strdup(&current->value[4]);
	current = env;
	while (ft_strncmp(current->value, "OLDPWD", 6) != 0)
		current = current->next;
	ft_strclr(&current->value[7]);
	current->value = ft_strjoindel(current->value, oldpwd);
	current = env;
	while (ft_strncmp(current->value, "PWD", 3) != 0)
		current = current->next;
	ft_strclr(&current->value[4]);
	current->value = ft_strjoindel(current->value, cdir);
	if (oldpwd != NULL)
		free(oldpwd);
}
