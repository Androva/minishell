/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/24 11:42:59 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/28 11:12:46 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_setenv(char **argv, t_env *env)
{
	char	*name;
	char	*value;
	t_env	*current;

	name = argv[1];
	value = argv[2];
	current = env;
	while (current->next != NULL)
	{
		if (ft_strncmp(current->value, name, ft_strlen(name)) == 0)
		{
			ft_strclr(&current->value[4]);
			ft_strcat(&current->value[4], value);
			return ;
		}
		current = current->next;
		if (current->next == NULL)
		{
			ft_strcat(name, "=");
			ft_strcat(name, value);
			current = envnew(name);
			envadd(&env, current);
		}
	}
}
