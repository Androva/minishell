# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/09/19 17:02:26 by nmatutoa          #+#    #+#              #
#    Updated: 2018/09/27 11:07:10 by nmatutoa         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = minishell
FLAGS = gcc -Wall -Werror -Wextra
SOURCES = struct.c minishell.c get_command.c exec_command.c exec_other.c echo.c cd.c pwd.c setenv.c unsetenv.c \
		  is_dir.c get_root.c \
		  envnew.c envadd.c envdel.c getenv.c envdup.c print_env.c cwd.c \

OBJECTS = $(SOURCES:.c=.o)

all: $(NAME)

$(NAME):
	@make -C libft
	@$(FLAGS) -c $(SOURCES)
	@$(FLAGS) -o $(NAME) $(OBJECTS) -L libft/ -lft
	@echo "Executable 'minishell' created"

clean:
	@/bin/rm -f $(OBJECTS)
	make clean -C libft
	@echo "minishell object files removed"

fclean: clean
	@/bin/rm -f minishell
	@make fclean -C libft
	@echo "Executable 'minishell' removed"

re: fclean all
