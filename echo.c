/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/20 11:22:21 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/28 11:04:44 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		is_quote(char c)
{
	if (c == 34 || c == 39)
		return (1);
	return (0);
}

void	env_echo(char *argv, t_env *env)
{
	t_env	*current;

	current = env;
	while (current != NULL &&
			ft_strncmp(current->value, argv, ft_strlen(argv)) != 0)
		current = current->next;
	if (current != NULL && current->value != NULL)
		ft_putstr(&current->value[ft_strlen(argv) + 1]);
	else
		ft_putstr("Variable not found.");
}

void	echo(char **argv, t_env *env)
{
	int	i;
	int	j;

	i = 0;
	if (ft_strcmp(argv[i], "echo") == 0)
		i++;
	while (argv[i] != NULL)
	{
		j = 0;
		if (ft_strchr(argv[i], '$') != NULL)
		{
			env_echo(&argv[i][1], env);
			break ;
		}
		while (argv[i][j] != '\0')
		{
			if (is_quote(argv[i][j]) == 0)
				ft_putchar(argv[i][j]);
			j++;
		}
		ft_putchar(' ');
		i++;
	}
	ft_putchar('\n');
}
