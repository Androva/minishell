/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/20 11:35:53 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/28 14:21:07 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	rootcd(char *argv, t_env *env, char **dir, char **cdir)
{
	char	buf[1024];
	char	*newdir;
	char	*temp_dir;

	temp_dir = get_root(env);
	while (ft_strlen(temp_dir) != ft_strlen(*cdir))
	{
		*cdir = getcwd(buf, 1024);
		if (ft_strlen(temp_dir) != ft_strlen(*cdir))
			chdir("../");
	}
	if (argv == NULL || ft_strcmp(argv, "~/") == 0)
		*dir = ft_strdup(*cdir);
	else
	{
		newdir = &argv[2];
		*dir = ft_strdup(*cdir);
		*dir = ft_strjoindel(*dir, "/");
		*dir = ft_strjoindel(*dir, newdir);
	}
	free(temp_dir);
}

void	change_dir(char *argv, char **dir, char **cdir)
{
	char	*changed;

	changed = NULL;
	if (ft_strchr(*dir, '~') == NULL && ft_strchr(argv, '-') == NULL)
		changed = ft_strjoin(*cdir, *dir);
	chdir(changed);
	free(changed);
}

void	dash_cd(char *argv, t_env *env, char **dir, char **cdir)
{
	t_env	*current;

	current = env;
	while (ft_strncmp(current->value, "OLDPWD", 6) != 0)
		current = current->next;
	rootcd(argv, env, dir, cdir);
	*dir = ft_strdup(&current->value[7]);
	chdir(*dir);
}

void	else_state(char *argv, char *dir, char *cdir, DIR *directory)
{
	if (dir[0] == '/')
		chdir(dir);
	else
		change_dir(argv, &dir, &cdir);
	closedir(directory);
}

void	cd(char **argv, t_env *env)
{
	char	buf[1024];
	char	*cdir;
	char	*dir;
	DIR		*directory;

	dir = NULL;
	cdir = getcwd(buf, 1024);
	cdir = ft_strcat(cdir, "/");
	if (argv[1] != NULL && ft_strcmp(argv[1], "-") == 0)
	{
		dash_cd(argv[1], env, &dir, &cdir);
		return ;
	}
	if (argv[1] == NULL || ft_strchr(argv[1], '~') != NULL)
		rootcd(argv[1], env, &dir, &cdir);
	else if (ft_strcmp(argv[1], "-") != 0)
		dir = ft_strdup(argv[1]);
	directory = opendir(dir);
	if (!directory)
		ft_printf("cd: no such file or directory: %s\n", argv[1]);
	else
		else_state(argv[1], dir, cdir, directory);
	cwd(env);
	free(dir);
}
