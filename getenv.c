/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/24 13:37:47 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/27 15:10:37 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_env	*ft_getenv(char **envp)
{
	t_env	*env;
	t_env	*head;
	int		i;
	char	cdir[1024];

	head = NULL;
	env = head;
	i = 0;
	getcwd(cdir, 1024);
	while (envp[i] != NULL)
	{
		env = envnew(envp[i]);
		envadd(&head, env);
		i++;
	}
	return (head);
}
