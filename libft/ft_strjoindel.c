/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoindel.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 15:45:08 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/27 15:49:08 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoindel(char const *s1, char const *s2)
{
	char	*join;

	join = ft_strjoin(s1, s2);
	free((char *)s1);
	return (join);
}
