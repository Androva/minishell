/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arr_dup.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/21 11:02:48 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/21 13:20:03 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**arr_dup(char **arr)
{
	int		i;
	int		len;
	char	**dup;

	i = 0;
	len = arrlen(arr);
	if (!(dup = (char **)malloc(sizeof(char *) * len + 1)))
		return (NULL);
	while (arr[i] != NULL)
	{
		dup[i] = NULL;
		dup[i] = ft_strdup(arr[i]);
		i++;
	}
	dup[i] = NULL;
	return (dup);
}
