/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unsetenv.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/24 15:10:25 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/28 14:20:13 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_unsetenv(char **argv, t_env **env)
{
	char	*name;
	t_env	*current;
	t_env	*prev;

	current = *env;
	prev = NULL;
	name = argv[1];
	while (current != NULL && current->value != NULL)
	{
		if (ft_strncmp(current->value, name, ft_strlen(name)) == 0)
		{
			if (prev == NULL)
				*env = current->next;
			else
				prev->next = current->next;
			free(current->value);
			free(current);
			return ;
		}
		if (current->next == NULL)
			return ;
		prev = current;
		current = current->next;
	}
	free(prev);
}
