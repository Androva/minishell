/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_command.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/19 17:10:55 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/28 14:02:56 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	get_command(char *str, t_command *command)
{
	if (ft_strcmp(str, "echo") == 0)
		command->echo += 1;
	else if (ft_strcmp(str, "cd") == 0)
		command->cd += 1;
	else if (ft_strcmp(str, "exit") == 0)
		command->exit += 1;
	else if (ft_strcmp(str, "setenv") == 0)
		command->setenv += 1;
	else if (ft_strcmp(str, "unsetenv") == 0)
		command->unsetenv += 1;
	else if (ft_strcmp(str, "env") == 0)
		command->env += 1;
	else if (ft_strcmp(str, "pwd") == 0)
		command->pwd += 1;
	else
	{
		if (is_dir(str) == 0)
			command->other = ft_strdup(str);
		else
			command->error += 1;
	}
}
