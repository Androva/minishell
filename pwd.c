/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pwd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/21 10:34:18 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/24 11:33:46 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	pwd(char **argv)
{
	char	buf[1024];

	if (argv[1] != NULL)
		ft_putendl("pwd: too many arguments");
	else
	{
		getcwd(buf, 1024);
		ft_putendl(buf);
	}
}
