/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_other.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 11:02:41 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/28 13:52:49 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	**get_path_env(t_env *env)
{
	t_env	*current;
	char	**env_path;

	current = env;
	env_path = NULL;
	while (ft_strncmp(current->value, "PATH", 4) != 0)
		current = current->next;
	env_path = ft_strsplit(&current->value[5], ':');
	if (!env_path)
		return (NULL);
	return (env_path);
}

char	*exec_location(char *path, t_env *env)
{
	char	**path_env;
	char	*exec;
	int		i;

	i = 0;
	if (!access(path, F_OK))
	{
		exec = ft_strdup(path);
		return (exec);
	}
	path_env = get_path_env(env);
	if (path_env == NULL)
		return (NULL);
	while (path_env[i] != NULL)
	{
		ft_strcat(path_env[i], "/");
		exec = ft_strjoin(path_env[i], path);
		if (!access(exec, F_OK))
			break ;
		free(exec);
		exec = NULL;
		i++;
	}
	free_arr(path_env);
	return (exec);
}

void	exec_other(char **argv, t_env *env, char **environ)
{
	char	*exec;

	exec = exec_location(argv[0], env);
	if (ft_strcmp(argv[0], "/usr/bin/env") == 0)
	{
		print_env(env);
		free(exec);
		return ;
	}
	if (!exec)
	{
		ft_printf("File %s not found\n", argv[0]);
		return ;
	}
	if (fork())
	{
		wait(NULL);
		free(exec);
	}
	else
		execve(argv[0], argv, environ);
}
