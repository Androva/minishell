/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   envnew.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/24 13:38:49 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/28 11:11:31 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_env	*envnew(char *envp)
{
	t_env	*env;

	if (!(env = (t_env *)ft_memalloc(sizeof(t_env))))
		return (NULL);
	if (envp)
		env->value = ft_strdup(envp);
	else
		env->value = NULL;
	env->next = NULL;
	return (env);
}
