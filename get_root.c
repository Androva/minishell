/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_root.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/25 11:39:17 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/26 16:13:48 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*get_root(t_env *env)
{
	char	*root;
	t_env	*current;

	current = env;
	while (ft_strncmp("HOME", current->value, 4) != 0)
		current = current->next;
	root = ft_strdup(&current->value[5]);
	return (root);
}
