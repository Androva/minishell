/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_dir.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/25 10:12:27 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/25 13:49:10 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		is_dir(char *name)
{
	struct stat	info;
	int			stat;

	stat = lstat(name, &info);
	if (S_ISREG(info.st_mode))
		return (0);
	else
	{
		if (S_ISDIR(info.st_mode))
			return (1);
		return (-1);
	}
	return (1);
}
