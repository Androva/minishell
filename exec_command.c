/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_command.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/19 17:16:51 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/28 10:55:21 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	exec_command(t_command command, char **arg, t_env **env, char **environ)
{
	if (command.exit != 0)
		exit(0);
	if (command.echo != 0)
		echo(arg, *env);
	if (command.cd != 0)
		cd(arg, *env);
	if (command.pwd != 0)
		pwd(arg);
	if (command.env != 0)
		print_env(*env);
	if (command.setenv != 0)
		ft_setenv(arg, *env);
	if (command.unsetenv != 0)
		ft_unsetenv(arg, env);
	if (command.other != NULL)
		exec_other(arg, *env, environ);
	if (command.error != 0)
	{
		ft_putstr("minishell: command not found: ");
		ft_putendl(arg[0]);
	}
}
