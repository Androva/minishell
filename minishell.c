/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/18 10:39:00 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/28 11:18:27 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	run(t_env *env, char **environ, char *line)
{
	char		**arg;
	t_command	command;

	init_command(&command);
	arg = ft_strsplit(line, ' ');
	get_command(arg[0], &command);
	exec_command(command, arg, &env, environ);
	free_arr(arg);
	free_other(&command);
}

int		main(int argc, char **argv, char **envp)
{
	char		*line;
	char		**environ;
	t_env		*env;

	argv = NULL;
	environ = arr_dup(envp);
	env = ft_getenv(envp);
	if (argc)
	{
		ft_putstr("( ͡° ͜ʖ ͡°) >: ");
		while (get_next_line(0, &line) > 0)
		{
			if (line != NULL && ft_strlen(line) != 0)
				run(env, environ, line);
			if (line != NULL)
				free(line);
			ft_putstr("( ͡° ͜ʖ ͡°) >: ");
		}
		env = envdel(&env);
		free_arr(environ);
	}
	return (0);
}
