/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   envdel.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/24 13:43:09 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/28 11:11:53 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_env	*envdel(t_env **head)
{
	t_env	*env;
	t_env	*temp;

	env = *head;
	while (env != NULL)
	{
		temp = env;
		env = env->next;
		if (temp->value != NULL)
			free(temp->value);
		free(temp);
		temp = NULL;
	}
	return (NULL);
}
