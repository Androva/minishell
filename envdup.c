/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   envdup.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 16:24:54 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/26 16:38:03 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_env	*envdup(t_env *env)
{
	t_env	*dup;
	t_env	*head;

	head = NULL;
	dup = head;
	while (env->next != NULL)
	{
		dup = envnew(env->value);
		envadd(&head, dup);
		env = env->next;
	}
	return (head);
}
