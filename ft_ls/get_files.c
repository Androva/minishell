/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_files.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/03 09:22:22 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/25 10:17:53 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_files	*get_files(char *name, t_flags flags, t_files *files)
{
	DIR				*dir;
	struct dirent	*file;
	struct stat		info;
	t_files			*head;
	t_files			*new;

	head = NULL;
	dir = opendir(name);
	lstat(name, &info);
	files = head;
	if (is_dir(name) != 1)
		return (NULL);
	while ((file = readdir(dir)) != NULL)
	{
		if (flags.a == 0)
		{
			while (file->d_name[0] == '.')
				file = readdir(dir);
		}
		new = lstnew(file->d_name);
		lstadd(&head, new);
	}
	closedir(dir);
	return (head);
}
