/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_permissions.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/03 13:41:19 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/07 13:08:16 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	*user_perm(struct stat info, char *perm)
{
	mode_t	permissions;

	permissions = info.st_mode & S_IRWXU;
	if (permissions)
	{
		if (info.st_mode & S_IRUSR)
			perm[1] = 'r';
		else
			perm[1] = '-';
		if (info.st_mode & S_IWUSR)
			perm[2] = 'w';
		else
			perm[2] = '-';
		if (info.st_mode & S_IXUSR)
			perm[3] = 'x';
		else
			perm[3] = '-';
	}
	else
		ft_strncpy(&perm[1], "---", 3);
	return (perm);
}

char	*group_perm(struct stat info, char *perm)
{
	mode_t	permissions;

	permissions = info.st_mode & S_IRWXG;
	if (permissions)
	{
		if (info.st_mode & S_IRGRP)
			perm[4] = 'r';
		else
			perm[4] = '-';
		if (info.st_mode & S_IWGRP)
			perm[5] = 'w';
		else
			perm[5] = '-';
		if (info.st_mode & S_IXGRP)
			perm[6] = 'x';
		else
			perm[6] = '-';
	}
	else
		ft_strncpy(&perm[4], "---", 3);
	return (perm);
}

char	*other_perm(struct stat info, char *perm)
{
	mode_t	permissions;

	permissions = info.st_mode & S_IRWXO;
	if (permissions)
	{
		if (info.st_mode & S_IROTH)
			perm[7] = 'r';
		else
			perm[7] = '-';
		if (info.st_mode & S_IWOTH)
			perm[8] = 'w';
		else
			perm[8] = '-';
		if (info.st_mode & S_IXOTH)
			perm[9] = 'x';
		else
			perm[9] = '-';
	}
	else
		ft_strncpy(&perm[7], "---", 3);
	return (perm);
}
