/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   count_size.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/04 14:02:58 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/07 13:23:26 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		count_size(t_files *files)
{
	int		count;

	count = 0;
	while (files != NULL)
	{
		count += files->blocks;
		files = files->next;
	}
	return (count);
}
