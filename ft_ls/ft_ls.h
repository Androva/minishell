/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 16:24:58 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/25 10:18:31 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include <unistd.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <dirent.h>
# include <time.h>
# include <uuid/uuid.h>
# include <pwd.h>
# include <grp.h>
# include <sys/xattr.h>
# include "../libft/libft.h"

typedef struct	s_files
{
	char			*filename;
	char			*user;
	char			*group;
	char			*permissions;
	int				size;
	int				blocks;
	int				links;
	char			*time;
	struct s_files	*next;
}				t_files;

typedef struct	s_flags
{
	int				a;
	int				l;
	int				r;
	int				t;
	int				error;
}				t_flags;

typedef struct	s_dir
{
	char			*directory;
	struct s_dir	*next;
}				t_dir;

void			ft_ls(char *dir, t_flags flags);
void			no_flags(t_files *files);
void			l_flag(t_files *files, char *directory);

t_files			*lstnew(char *filename);
void			lstadd(t_files **lst, t_files *new);
t_files			*lstdel(t_files **files);
void			swap(char **a, char **b);
void			lstsort(t_files *list);
void			rlstsort(t_files *list);
void			tlstsort(t_files *list, t_files *head);
void			trlstsort(t_files *list, t_files *head);
void			flags_init(t_flags *flags);
t_dir			*dirnew(char *directory);
void			diradd(t_dir **lst, t_dir *new);
t_dir			*dirdel(t_dir **head);
void			dirsort(t_dir *list);
void			dirrsort(t_dir *list);
void			sortdir(t_dir *dir, t_flags flags);

int				is_dir(char *name);
t_files			*get_files(char *name, t_flags flags, t_files *files);
t_files			*get_stats(t_files *files, char *directory);
t_files			*get_user(t_files *files, char *directory);
t_files			*get_group(t_files *files, char *directory);
t_files			*get_perm(t_files *files, char *directory);
char			*user_perm(struct stat info, char *perm);
char			*group_perm(struct stat info, char *perm);
char			*other_perm(struct stat info, char *perm);
int				count_size(t_files *files);

#endif
