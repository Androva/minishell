/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lstdel.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/08 12:26:35 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/10 10:36:28 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_files	*lstdel(t_files **head)
{
	t_files	*files;
	t_files	*temp;

	files = *head;
	while (files != NULL)
	{
		temp = files;
		files = files->next;
		if (temp->filename != NULL)
			free(temp->filename);
		if (temp->user != NULL)
			free(temp->user);
		if (temp->group != NULL)
			free(temp->group);
		if (temp->permissions != NULL)
			free(temp->permissions);
		if (temp->time != NULL)
			free(temp->time);
		free(temp);
		temp = NULL;
	}
	return (NULL);
}
