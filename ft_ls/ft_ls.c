/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 16:23:35 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/25 13:31:51 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_flags	set_flags(char *str, t_flags flags)
{
	str++;
	while (*str != '\0')
	{
		if (*str == 'a')
			flags.a += 1;
		else if (*str == 'l')
			flags.l += 1;
		else if (*str == 'r')
			flags.r += 1;
		else if (*str == 't')
			flags.t += 't';
		else
		{
			flags.error += 1;
			break ;
		}
		str++;
	}
	if (flags.error != 0)
	{
		ft_putendl("Incorrect flag used.");
		exit(0);
	}
	return (flags);
}

void	ft_ls(char *dir, t_flags flags)
{
	t_files	*files;
	t_files	*head;
	char	*directory;

	files = NULL;
	head = files;
	directory = ft_strjoin(dir, "/");
	files = get_files(directory, flags, files);
	if (flags.r != 0)
	{
		if (flags.t == 0)
			rlstsort(files);
		else
			trlstsort(files, head);
	}
	else if (flags.t != 0 && flags.r == 0)
		tlstsort(files, head);
	else
		lstsort(files);
	if (flags.l == 0)
		no_flags(files);
	if (flags.l != 0)
		l_flag(files, directory);
	free(directory);
	files = lstdel(&files);
}

void	multi_dir(t_flags flags, int i, int argc, char **argv)
{
	t_dir	*dir;
	t_dir	*new;
	t_dir	*head;

	head = NULL;
	new = head;
	while (i < argc)
	{
		new = dirnew(argv[i]);
		diradd(&head, new);
		i++;
	}
	dir = head;
	sortdir(head, flags);
	while (dir != NULL)
	{
		ft_putstr(dir->directory);
		ft_putendl(":");
		ft_ls(dir->directory, flags);
		ft_putchar('\n');
		dir = dir->next;
	}
	head = dirdel(&head);
}

int		main(int argc, char **argv)
{
	t_flags	flags;
	int		i;

	flags_init(&flags);
	i = 1;
	if (argc)
	{
		while (i < argc && argv[i][0] == '-')
		{
			flags = set_flags(argv[i], flags);
			i++;
		}
		if (argv[i] == NULL)
			ft_ls(".", flags);
		if (argv[i + 1] != NULL)
			multi_dir(flags, i, argc, argv);
		else
			ft_ls(argv[i], flags);
	}
	return (0);
}
