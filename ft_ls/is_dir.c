/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_dir.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/25 10:12:27 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/25 13:48:41 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"
#include <errno.h>

int		is_dir(char *name)
{
	struct stat	info;
	int			stat;
	char		*file;

	file = ft_strnew(ft_strlen(name) - 1);
	file = ft_strncpy(file, name, ft_strlen(name) - 1);
	stat = lstat(file, &info);
	if (S_ISREG(info.st_mode))
	{
		ft_putstr(file);
		free(file);
		return (0);
	}
	else
	{
		if (S_ISDIR(info.st_mode))
			return (1);
		ft_printf("No such file or directory: (%s)", file);
		free(file);
		return (-1);
	}
	free(file);
	return (1);
}
