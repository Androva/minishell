/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: nmatutoa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/18 10:39:31 by nmatutoa          #+#    #+#             */
/*   Updated: 2018/09/28 11:52:26 by nmatutoa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "ft_ls/ft_ls.h"
# include "libft/libft.h"
# include <signal.h>
# include <sys/wait.h>
# include <sys/types.h>

typedef struct	s_command
{
	int		echo;
	int		cd;
	int		exit;
	int		setenv;
	int		unsetenv;
	int		env;
	int		pwd;
	char	*other;
	int		error;
}				t_command;

typedef struct	s_env
{
	char			*value;
	struct s_env	*next;
}				t_env;

t_env	*envnew(char *envp);
void	envadd(t_env **lst, t_env *new);
t_env	*envdel(t_env **head);
void	print_env(t_env *env);
t_env	*envdup(t_env *env);
t_env	*ft_getenv(char **envp);

void	echo(char **argv, t_env *env);
void	cd(char **argv, t_env *env);
void	pwd(char **argv);
void	ft_setenv(char **argv, t_env *env);
void	ft_unsetenv(char **argv, t_env **env);

void	init_command(t_command *command);
void	free_other(t_command *command);
void	get_command(char *str, t_command *command);
void	exec_command(t_command command, char **arg,
		t_env **env, char **environ);
void	exec_other(char **argv, t_env *env, char **environ);

int		is_dir(char *str);
char	*get_root(t_env *env);
void	cwd(t_env *env);

#endif
